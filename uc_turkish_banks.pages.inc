<?php

/**
 * @file
 * Turkish Bank Gateway menu items.
 */

/**
 * Finalizes transaction for 3d payment method.
 */
function uc_turkish_banks_3d_callback($cart_id = 0) {
  watchdog('uc_turkish_banks', 'Receiving new order notification for order !order_id.', array(
      '!order_id' => check_plain($_POST['oid'])
  ), WATCHDOG_INFO);
  global $user;
  $order = uc_order_load($_POST['oid']);
  $bank_key = $_POST["bankkey"];
  $banks = _get_bank_list();
  if ($banks[$bank_key][1] == 'est') {
    $md_status = $_POST["mdStatus"];
    $err_msg = $_POST["mdErrorMsg"];
  }
  elseif ($banks[$bank_key][1] == 'gvp') {
    $md_status = $_POST["mdstatus"];
    $err_msg = $_POST["mderrormessage"];
  }
  
  if ($md_status == 1 || $md_status == 2 || $md_status == 3 || $md_status == 4) {
    
    if ($banks[$bank_key][1] == 'est') {
      
      $hashparams = $_POST["HASHPARAMS"];
      $hashparamsval = $_POST["HASHPARAMSVAL"];
      $hashparam = $_POST["HASH"];
      $storekey = variable_get('3d_storekey_' . $bank_key);
      $paramsval = "";
      $index1 = 0;
      $index2 = 0;
      
      while ($index1 < strlen($hashparams)) {
        $index2 = strpos($hashparams, ":", $index1);
        $vl = $_POST[substr($hashparams, $index1, $index2 - $index1)];
        if ($vl == NULL)
          $vl = "";
        $paramsval = $paramsval . $vl;
        $index1 = $index2 + 1;
      }
      $hashval = $paramsval . $storekey;
      
      $hash = base64_encode(pack('H*', sha1($hashval)));
      
      if ($paramsval != $hashparamsval || $hashparam != $hash) {
        uc_order_comment_save($order->order_id, 0, t('Güvenlik uyarısı. Sayısal imza geçerli değil'), 'admin');
      }
      
      // /////////////////////////////////////////////
      if (!function_exists('curl_init')) {
        drupal_set_message(t('The Bank service requires cURL.  Please talk to your system administrator to get this configured.'));
        return array(
            'success' => FALSE
        );
      }
      
      // $order = uc_order_load($_SESSION['cart_order']);
      
      // XML request sablonu
      $request = "DATA=<?xml version=\"1.0\" encoding=\"ISO-8859-9\"?>" . "<CC5Request>" . "<Name>{APIUSER}</Name>" . "<Password>{APIPASSWORD}</Password>" . "<ClientId>{CLIENTID}</ClientId>" . "<IPAddress>{IP}</IPAddress>" . "<oid>{OID}</oid>" . "<Type>Auth</Type>" . "<Number>{NUMBER}</Number>" . "<Amount>{AMOUNT}</Amount>" . "<Currency>949</Currency>" . "<PayerTxnId>{PAYERTXNID}</PayerTxnId>" . "<PayerSecurityLevel>{PAYERSECLEVEL}</PayerSecurityLevel>" . "<PayerAuthenticationCode>{PAYERAUTHCODE}</PayerAuthenticationCode>" . "</CC5Request>";
      
      $request = str_replace("{APIUSER}", variable_get('3d_api_username_' . $bank_key), $request);
      $request = str_replace("{APIPASSWORD}", variable_get('3d_api_password_' . $bank_key), $request);
      $request = str_replace("{CLIENTID}", variable_get('3d_clientid_' . $bank_key), $request);
      $request = str_replace("{IP}", ip_address(), $request);
      $request = str_replace("{OID}", $_POST['oid'], $request);
      $request = str_replace("{NUMBER}", $_POST['md'], $request);
      $request = str_replace("{AMOUNT}", $_POST['amount'], $request);
      $request = str_replace("{PAYERTXNID}", $_POST['xid'], $request);
      $request = str_replace("{PAYERSECLEVEL}", $_POST['eci'], $request);
      $request = str_replace("{PAYERAUTHCODE}", $_POST['cavv'], $request);
      
      // TODO: ?? bu ne olacak? $request=str_replace("{TAKSIT}",$taksit,$request);
      
      $url = variable_get('3d_api_url_' . $bank_key);
      
      watchdog('uc_turkish_banks', 'Request:' . $request, array(), WATCHDOG_INFO);
      
      $ch = curl_init(); // initialize curl handle
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_SSLVERSION, 3);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
      curl_setopt($ch, CURLOPT_TIMEOUT, 90); // times out after 4s
      curl_setopt($ch, CURLOPT_POSTFIELDS, $request); // add POST fields
      
      $result = curl_exec($ch); // run the whole process
      watchdog('uc_turkish_banks', 'Response:' . $result, array(), WATCHDOG_INFO);
      if ($error = curl_error($ch)) {
        watchdog('uc_turkish_banks', $error, array(), WATCHDOG_ERROR);
      }
      curl_close($ch);
      
      $response = "";
      $order_id = "";
      $auth_code = "";
      $proc_return_code = "";
      $err_msg = "";
      $host_msg = "";
      
      $response_tag = "Response";
      $posf = strpos($result, ("<" . $response_tag . ">"));
      $posl = strpos($result, ("</" . $response_tag . ">"));
      $posf = $posf + strlen($response_tag) + 2;
      $response = substr($result, $posf, $posl - $posf);
      
      $response_tag = "OrderId";
      $posf = strpos($result, ("<" . $response_tag . ">"));
      $posl = strpos($result, ("</" . $response_tag . ">"));
      $posf = $posf + strlen($response_tag) + 2;
      $order_id = substr($result, $posf, $posl - $posf);
      
      $response_tag = "AuthCode";
      $posf = strpos($result, "<" . $response_tag . ">");
      $posl = strpos($result, "</" . $response_tag . ">");
      $posf = $posf + strlen($response_tag) + 2;
      $auth_code = substr($result, $posf, $posl - $posf);
      
      $response_tag = "ProcReturnCode";
      $posf = strpos($result, "<" . $response_tag . ">");
      $posl = strpos($result, "</" . $response_tag . ">");
      $posf = $posf + strlen($response_tag) + 2;
      $proc_return_code = substr($result, $posf, $posl - $posf);
      
      $response_tag = "ErrMsg";
      $posf = strpos($result, "<" . $response_tag . ">");
      $posl = strpos($result, "</" . $response_tag . ">");
      $posf = $posf + strlen($response_tag) + 2;
      $err_msg = substr($result, $posf, $posl - $posf);
      
      if ($response != 'Approved') {
        $message = t('Odemeniz <b><font color="RED">GERÇEKLEŞEMEMİŞTİR!</font></b>. Lütfen sepetinizi kontrol ederek tekrar deneyiniz.');
        $message .= t('<br/><b>Detay:</b>' . $proc_return_code . ':' . $err_msg . '.');
        drupal_set_message(check_plain($message));
        $_SESSION['do_review'] = TRUE;
        drupal_goto('cart/checkout/review');
      }
      else {
        $message = t('Credit card charged: !amount', array(
            '!amount' => uc_currency_format($_POST['amount'])
        ));
        uc_payment_enter($order->order_id, t('Anlaşmalı Banka'), $_POST["amount"], 0, NULL, $message);
        uc_order_comment_save($order->order_id, 0, $message, 'admin');
        $_SESSION['do_complete'] = TRUE;
        $message = t('Alış veriş için sitemizi seçtiğiniz için teşekkür ederiz.');
        $message .= '<br>';
        $message .= t('İşlem numaranız: ' . $order_id);
        $message .= '<br>';
        $message .= t('Otorizasyon kodunuz: ' . $auth_code);
        $message .= '<br>';
        $message .= t('Tahsilat tutarı: ' . uc_currency_format($_POST['amount']));
        // TODO: Buraya daha sonra oturum açarak yaptığı işlemin detaylarını tâkip edebileceği bilgisi yazılmalı
        return $message;
      }
    }
    elseif ($banks[$bank_key][1] == 'gvp') {
      $str_mode = $_POST['mode'];
      $str_version = $_POST['apiversion'];
      $str_terminal_id = $_POST['clientid'];
      $str_terminal_id_ = "0" . $_POST['clientid'];
      $str_provision_password = variable_get('3d_api_password_' . $bank_key);
      $str_prov_user_id = $_POST['terminalprovuserid'];
      $str_user_id = $_POST['terminaluserid'];
      $str_merchant_id = $_POST['terminalmerchantid'];
      $str_ip_address = $_POST['customeripaddress'];
      $str_email_address = $_POST['customeremailaddress'];
      $str_order_id = $_POST['orderid'];
      $str_amount = $_POST['txnamount'];
      $str_currency_code = $_POST['txncurrencycode'];
      $str_installment_count = $_POST['txninstallmentcount'];
      $str_cardholder_present_code = "13"; // 3D Model işlemde bu değer 13 olmalı
      $str_type = $_POST['txntype'];
      $str_motoInd = "N";
      $str_authentication_code = $_POST['cavv'];
      $str_security_level = $_POST['eci'];
      $str_txn_id = $_POST['xid'];
      $str_md = $_POST['md'];
      $security_data = strtoupper(sha1($str_provision_password . $str_terminal_id_));
      $hash_data = strtoupper(sha1($str_order_id . $str_terminal_id . $str_amount . $security_data));
      $str_host_address = variable_get('3d_api_url_' . $bank_key);
      
      $str_xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
          <GVPSRequest>
          <Mode>$str_mode</Mode>
          <Version>$str_version</Version>
          <Terminal><ProvUserID>$str_prov_user_id</ProvUserID><HashData>$hash_data</HashData><UserID>$str_user_id</UserID><ID>$str_terminal_id</ID><MerchantID>$str_merchant_id</MerchantID></Terminal>
          <Customer><IPAddress>$str_ip_address</IPAddress><EmailAddress>$str_email_address</EmailAddress></Customer>
          <Order><OrderID>$str_order_id</OrderID><GroupID></GroupID></Order>
          <Transaction><Type>$str_type</Type><InstallmentCnt>$str_installment_count</InstallmentCnt><Amount>$str_amount</Amount><CurrencyCode>$str_currency_code</CurrencyCode><CardholderPresentCode>$str_cardholder_present_code</CardholderPresentCode><MotoInd>$str_motoInd</MotoInd><Secure3D><AuthenticationCode>$str_authentication_code</AuthenticationCode><SecurityLevel>$str_security_level</SecurityLevel><TxnID>$str_txn_id</TxnID><Md>$str_md</Md></Secure3D>
          </Transaction>
          </GVPSRequest>";
      
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $str_host_address);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "data=" . $str_xml);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      $results = curl_exec($ch);
      curl_close($ch);
      
      echo "<b>Giden istek </b><br />";
      echo $str_xml;
      echo "<br /><b>Gelen Yanıt </b><br />";
      echo $results;
      
      foreach ($_POST as $key => $value) {
        echo "<br>" . $key . " : " . $value;
      }
    }
  }
  else {
    $message = t('Ödemeniz <b><font color="RED">GERÇEKLEŞEMEMİŞTİR</font></b>. Lütfen ödeme bilgilerinizi kontrol ederek tekrar deneyiniz.');
    $message .= t('<br/><b>Detay:</b> MDStatus:' . $md_status . ':' . $err_msg . '.');
    drupal_set_message(check_plain($message));
    $_SESSION['do_review'] = TRUE;
    drupal_goto('cart/checkout/review');
  }
}
